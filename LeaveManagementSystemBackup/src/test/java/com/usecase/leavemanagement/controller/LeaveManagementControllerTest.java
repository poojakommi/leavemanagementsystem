package com.usecase.leavemanagement.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.usecase.leavemanagement.dto.ResponseDto;
import com.usecase.leavemanagement.exception.LeaveException;
import com.usecase.leavemanagement.service.ApplyForLeavesService;
import com.usecase.leavemanagement.service.GetLeavesService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class LeaveManagementControllerTest {

	@Mock
	GetLeavesService leavesService;

	@Mock
	ApplyForLeavesService applyForLeaveService;

	@InjectMocks
	LeaveManagementController leaveManagementController;

	int userId, leaveId, leavesApplied;

	ResponseDto responseDto;
	int statusCode = 200;
	String message = "Successful";

	ResponseEntity<ResponseDto> resp;

	@Before
	public void Setup() {
		responseDto = new ResponseDto();

		userId = 1;
		leaveId = 1;
		leavesApplied = 1;
		responseDto.setStatusCode(statusCode);
		responseDto.setMessage(message);

	}

	@Test
	public void getLeaves() throws LeaveException {
		Mockito.when(leavesService.getLeavesListForParticularUser(Mockito.anyInt())).thenReturn(responseDto);
		ResponseDto actualValue = leaveManagementController.getLeavesListForParticularUser(userId);
		Assert.assertEquals("Successful", actualValue.getMessage());
	}

	@Test
	public void applyLeave() {
		Mockito.when(applyForLeaveService.applyForLeave(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt()))
				.thenReturn(responseDto);
		ResponseEntity<ResponseDto> actualValue = leaveManagementController.applyForLeave(userId, leaveId, leavesApplied);
		Assert.assertEquals(201, actualValue.getStatusCodeValue());
	}
}


