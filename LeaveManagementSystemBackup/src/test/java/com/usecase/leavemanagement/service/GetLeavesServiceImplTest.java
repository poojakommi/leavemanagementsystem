package com.usecase.leavemanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.usecase.leavemanagement.dto.ResponseDto;
import com.usecase.leavemanagement.entity.Leave;
import com.usecase.leavemanagement.exception.LeaveException;
import com.usecase.leavemanagement.repositary.LeaveRepository;

public class GetLeavesServiceImplTest {

	@Mock
	LeaveRepository leaveRepositary;

	@InjectMocks
	GetLeavesServiceImpl getLeaveService;

	ResponseDto responseDto;
	int userId;
	Leave leave;
	List<Leave> listOfLeaves;

	@Before
	public void setup() {
		userId = 1;
		responseDto = new ResponseDto();
		listOfLeaves = new ArrayList<Leave>();
		leave = new Leave();
		listOfLeaves.add(leave);
	}

	@Test
	public void getLeaves() throws LeaveException {
		Mockito.when(leaveRepositary.findByUser_userId(Mockito.anyInt())).thenReturn(listOfLeaves);
		ResponseDto actualValue = getLeaveService.getLeavesListForParticularUser(userId);
		Assert.assertEquals("Successful", actualValue.getMessage());
	}

}
