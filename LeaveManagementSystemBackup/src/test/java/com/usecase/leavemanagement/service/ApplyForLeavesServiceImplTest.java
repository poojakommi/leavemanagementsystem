package com.usecase.leavemanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.usecase.leavemanagement.dto.ResponseDto;
import com.usecase.leavemanagement.entity.UserLeave;
import com.usecase.leavemanagement.exception.LeaveException;
import com.usecase.leavemanagement.repositary.UserLeaveRepository;

public class ApplyForLeavesServiceImplTest {

	@Mock
	UserLeaveRepository userLeaveRepository;

	@InjectMocks
	ApplyForLeavesServiceImpl applyForLeaveService;

	ResponseDto responseDto;
	int userId, leaveId, leavesApplied;
	UserLeave userLeave;

	@Before
	public void setup() {
		userId = 1;
		leaveId = 1;
		leavesApplied = 1;
		userLeave = new UserLeave();
		responseDto = new ResponseDto();
	}

	@Test
	public void getLeaves() throws LeaveException {
		Mockito.when(userLeaveRepository.save(Mockito.any(UserLeave.class))).thenReturn(userLeave);
		ResponseDto actualValue = applyForLeaveService.applyForLeave(userId, leaveId, leavesApplied);
		Assert.assertEquals("Successful", actualValue.getMessage());
	}

}
