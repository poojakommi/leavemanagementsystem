package com.usecase.leavemanagement.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The UserLeave entity is used to create a table in the database.
 * @author Pooja.Kommi
 *
 */
@Entity
@Table(name = "USER_LEAVES")
public class UserLeave implements Serializable {
	
	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true)
	private int id;
	
	@Column(name = "USER_ID", unique = false)
	private int userId;
	
	@Column(name = "LEAVE_ID", unique = false)
	private int leaveId;
	
	@Column(name = "DAYS_APPLIED", unique = false)
	private int daysApplied;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}
	public int getDaysApplied() {
		return daysApplied;
	}
	public void setDaysApplied(int daysApplied) {
		this.daysApplied = daysApplied;
	}
	public UserLeave() {
		super();
	}


	
}
