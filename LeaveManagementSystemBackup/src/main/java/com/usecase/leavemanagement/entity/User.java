package com.usecase.leavemanagement.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The User entity is used to create a table in the database.
 * @author Pooja.Kommi
 *
 */
@Entity
@Table(name = "USER")
public class User implements Serializable {
	// object is converted to byte stream

	//Don't want to insert into DB.
	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "USER_ID", unique = true)
	private int userId;

	@Column(name = "USER_NAME", unique = false, length = 30)
	private String userName;

	@Column(name = "IS_LOGGED_IN", unique = false)
	private Boolean isLoggedIn;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Boolean getIsLoggedIn() {
		return isLoggedIn;
	}

	

	public User(Set<Leave> leaves) {
		super();
		this.leaves = leaves;
	}

	public void setIsLoggedIn(Boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}

	

	
	public Set<Leave> getLeaves() {
		return leaves;
	}

	public void setLeaves(Set<Leave> leaves) {
		this.leaves = leaves;
	}

	public User() {
		super();
	}

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "user")
	private Set<Leave> leaves;

}
