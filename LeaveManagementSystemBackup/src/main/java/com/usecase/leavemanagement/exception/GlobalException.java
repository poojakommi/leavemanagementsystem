package com.usecase.leavemanagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jca.cci.core.RecordCreator;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * This class is where all the exceptions are defined.
 * @author pooja.kommi
 *
 */
@ControllerAdvice
public class GlobalException extends ResponseEntityExceptionHandler {

	@ExceptionHandler(LeaveException.class)
	public ResponseEntity<ErrorResponse> leaveException(LeaveException le) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(le.getMessage());
		errorResponse.setStatus(HttpStatus.NOT_FOUND.value());

		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.NOT_FOUND);

	}
	
	
	
	@ExceptionHandler(RecordNotFoundException.class)
	public ResponseEntity<ErrorResponse> RecordNotFoundException(RecordNotFoundException rnfe) {
		ErrorResponse errorResponse = new ErrorResponse();

		errorResponse.setMessage(rnfe.getMessage());
		errorResponse.setStatus(HttpStatus.NOT_FOUND.value());

		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.NOT_FOUND);

	}
}
