package com.usecase.leavemanagement.exception;

/**
 * This is the POJO used to return error message and status.
 * @author pooja.kommi
 *
 */
public class ErrorResponse {
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private int status;
	String message;

}
