package com.usecase.leavemanagement.exception;

/**
 * Leave exception is to initialize the exception using a single parameter constructor. 
 * @author pooja.kommi
 *
 */
public class LeaveException extends Exception {

	private static final long serialVersionUID = -3193616515580108650L;

	String message;

	public LeaveException(String message) {
		super(message);
	}

}
