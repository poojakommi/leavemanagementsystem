package com.usecase.leavemanagement.exception;


/**
 * RecordNotFound exception is to initialize the exception using a single parameter constructor. 
 * @author pooja.kommi
 *
 */
//@ResponseStatus(HttpStatus.NOT_FOUND)
public class RecordNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -3193616515580108650L;

	public RecordNotFoundException(String message) {
		super(message);
	}

}
