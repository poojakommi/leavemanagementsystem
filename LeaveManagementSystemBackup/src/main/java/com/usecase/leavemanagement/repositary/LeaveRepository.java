package com.usecase.leavemanagement.repositary;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.usecase.leavemanagement.entity.Leave;

/**
 * This repository extends JPA Repository so as to be able to access all the methods 
 * that JPA repository provides.
 * @author pooja.kommi
 *
 */
public interface LeaveRepository extends JpaRepository<Leave, Integer> {

	List<Leave> findByUser_userId(int id);

	Optional<Leave> findByLeaveId(int leaveId);

}
