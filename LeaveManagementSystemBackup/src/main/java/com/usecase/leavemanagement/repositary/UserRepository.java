package com.usecase.leavemanagement.repositary;

import org.springframework.data.jpa.repository.JpaRepository;

import com.usecase.leavemanagement.entity.User;

/**
 * This repository extends JPA Repository so as to be able to access all the methods 
 * that JPA repository provides.
 * @author pooja.kommi
 *
 */
public interface UserRepository extends JpaRepository<User, Integer> {

}
