package com.usecase.leavemanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.usecase.leavemanagement.dto.ResponseDto;
import com.usecase.leavemanagement.exception.LeaveException;
import com.usecase.leavemanagement.service.ApplyForLeavesService;
import com.usecase.leavemanagement.service.GetLeavesService;

@RestController
public class LeaveManagementController {

	// goes to implementation of this interface.
	@Autowired
	GetLeavesService leavesService;

	@Autowired
	ApplyForLeavesService applyForLeaveService;

	@Autowired
	RestTemplate restTemplate;

	/**
	 * @author pooja.kommi
	 * @param userId
	 * @param leaveId
	 * @param leavesApplied
	 * @return
	 */
	@PostMapping("/applyLeaves/{userId}/{leaveId}/{leavesApplied}")
	public ResponseEntity<ResponseDto> applyForLeave(@PathVariable(name = "userId") int userId,
			@PathVariable(name = "leaveId") int leaveId, @PathVariable(name = "leavesApplied") int leavesApplied) {
		ResponseDto responseDto = applyForLeaveService.applyForLeave(userId, leaveId, leavesApplied);
		return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
	}

	/**
	 * @author pooja.kommi
	 * @param userId
	 * @return
	 * @throws LeaveException
	 */
	@GetMapping("/leaves/{userId}")
	public ResponseDto getLeavesListForParticularUser(@PathVariable(name = "userId") int userId) throws LeaveException {
		ResponseDto responsedto = leavesService.getLeavesListForParticularUser(userId);
		return responsedto;
	}
}
