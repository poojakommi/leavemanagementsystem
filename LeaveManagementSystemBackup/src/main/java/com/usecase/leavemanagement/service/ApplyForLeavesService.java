package com.usecase.leavemanagement.service;


import org.springframework.stereotype.Service;

import com.usecase.leavemanagement.dto.ResponseDto;
import com.usecase.leavemanagement.exception.RecordNotFoundException;

@Service
public interface ApplyForLeavesService {

	public ResponseDto applyForLeave(int userId, int leaveId, int leavesApplied) throws RecordNotFoundException;


}
