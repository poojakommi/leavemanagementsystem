package com.usecase.leavemanagement.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.usecase.leavemanagement.controller.LeaveManagementController;
import com.usecase.leavemanagement.dto.ResponseDto;
import com.usecase.leavemanagement.entity.Leave;
import com.usecase.leavemanagement.entity.UserLeave;
import com.usecase.leavemanagement.exception.RecordNotFoundException;
import com.usecase.leavemanagement.repositary.LeaveRepository;
import com.usecase.leavemanagement.repositary.UserLeaveRepository;
import java.util.Optional;
import org.apache.log4j.Logger;

 
@Service
public class ApplyForLeavesServiceImpl implements ApplyForLeavesService {

	Logger log = Logger.getLogger(LeaveManagementController.class);

	@Autowired
	LeaveRepository leaveRepositary;

	@Autowired
	UserLeaveRepository userLeaveRepository;

	@Autowired
	RestTemplate restTemplate;

	/**
	 * @author pooja.kommi
	 * @param userId
	 * @param leaveId
	 * @param leavesApplied
	 * This method is used for the user to apply for leave.
	 * If there don't exist enough leaves for that particular user, then an error is displayed.
	 */
	@Override
	public ResponseDto applyForLeave(int userId, int leaveId, int leavesApplied) throws RecordNotFoundException {
		
		UserLeave userLeave = new UserLeave();
		ResponseDto responseDto = new ResponseDto();
		userLeave.setUserId(userId);
		userLeave.setLeaveId(leaveId);
		userLeave.setDaysApplied(leavesApplied);
		BeanUtils.copyProperties(userLeave, responseDto);

		if (userLeaveRepository.save(userLeave) == null) {
			log.error("Error occured while trying to register!");
			throw new RecordNotFoundException("Error occured while trying to register!");

		} else {
			
			responseDto.setStatusCode(HttpStatus.OK.value());
			responseDto.setMessage("Leave Registered SuccessFully");
			Optional<Leave> leave = leaveRepositary.findByLeaveId(leaveId);
			if(leave.get().getAvailableLeaves()>=leavesApplied)
			leave.get().setAvailableLeaves(leave.get().getAvailableLeaves()-leavesApplied);
			log.info("User successfully applied for leave!");
		}
		return responseDto;

	}
}






/*
 * package com.usecase.leavemanagement.service;
 * 
 * import org.springframework.beans.BeanUtils; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.http.HttpStatus; import
 * org.springframework.stereotype.Service; import
 * org.springframework.web.client.RestTemplate;
 * 
 * import com.usecase.leavemanagement.controller.LeaveManagementController;
 * import com.usecase.leavemanagement.dto.ResponseDto; import
 * com.usecase.leavemanagement.entity.Leave; import
 * com.usecase.leavemanagement.entity.UserLeave; import
 * com.usecase.leavemanagement.repositary.LeaveRepository; import
 * com.usecase.leavemanagement.repositary.UserLeaveRepository; import
 * java.util.Optional; import org.apache.log4j.Logger;
 * 
 * 
 * @Service public class ApplyForLeavesServiceImpl implements
 * ApplyForLeavesService {
 * 
 * Logger log = Logger.getLogger(LeaveManagementController.class);
 * 
 * @Autowired LeaveRepository leaveRepositary;
 * 
 * @Autowired UserLeaveRepository userLeaveRepository;
 * 
 * @Autowired RestTemplate restTemplate;
 * 
 * @Override public ResponseDto applyForLeave(int userId, int leaveId, int
 * leavesApplied) {
 * 
 * UserLeave userLeave = new UserLeave(); ResponseDto responseDto = new
 * ResponseDto();
 * 
 * userLeave.setUserId(userId); userLeave.setLeaveId(leaveId);
 * userLeave.setDaysApplied(leavesApplied);
 * 
 * 
 * BeanUtils.copyProperties(userLeave, responseDto);
 * 
 * Optional<Leave> leave = leaveRepositary.findByLeaveId(leaveId);
 * if(leave.get().getAvailableLeaves()>=leavesApplied)
 * leave.get().setAvailableLeaves(leave.get().getAvailableLeaves()-leavesApplied
 * );
 * 
 * if (userLeaveRepository.save(userLeave) != null) {
 * responseDto.setStatusCode(HttpStatus.OK.value());
 * responseDto.setMessage("Leave Registered SuccessFully");
 * log.info("User successfully applied for leave!");
 * 
 * } else { responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
 * responseDto.setMessage("Something went wrong. Please try again");
 * log.error("Error occured while trying to register!"); } return responseDto;
 * 
 * } }
 */


/*
 * Optional<Leave> leave = leaveRepositary.findByLeaveId(leaveId);
 * if(leave.get().getAvailableLeaves()>=leavesApplied)
 * leave.get().setAvailableLeaves(leave.get().getAvailableLeaves()-leavesApplied
 * );
 */
