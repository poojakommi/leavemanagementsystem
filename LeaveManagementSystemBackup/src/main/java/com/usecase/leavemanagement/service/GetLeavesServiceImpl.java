package com.usecase.leavemanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.usecase.leavemanagement.controller.LeaveManagementController;
import com.usecase.leavemanagement.dto.LeaveDto;
import com.usecase.leavemanagement.dto.ResponseDto;
import com.usecase.leavemanagement.entity.Leave;
import com.usecase.leavemanagement.exception.LeaveException;
import com.usecase.leavemanagement.repositary.LeaveRepository;
import com.usecase.leavemanagement.repositary.UserLeaveRepository;

@Service
public class GetLeavesServiceImpl implements GetLeavesService {

	Logger log = Logger.getLogger(LeaveManagementController.class);

	@Autowired
	LeaveRepository leaveRepositary;

	@Autowired
	UserLeaveRepository userLeaveRepository;

	@Autowired
	RestTemplate restTemplate;


	
	
	/**
	 * @author pooja.kommi
	 * This method is used for the user to get a list of all the the leaves of a particular user.
	 * If it doesn't exist,then an error is displayed.
	 */
	@Override
	public ResponseDto getLeavesListForParticularUser(int id) throws LeaveException {

		ResponseDto responseDto = new ResponseDto();

		List<Leave> listOfLeavesForParticularUser = leaveRepositary.findByUser_userId(id);

		List<LeaveDto> leaveDto = new ArrayList<>();

		for (Leave leave : listOfLeavesForParticularUser) {
			LeaveDto leaveDtoTemp = new LeaveDto();
			BeanUtils.copyProperties(leave, leaveDtoTemp);
			leaveDto.add(leaveDtoTemp);
		}

		if (leaveDto.isEmpty()) {
			log.error("Could not retrieve list of leaves as user does not exist");
			throw new LeaveException("This user does not exist!");
		}

		else {
			log.info("successfully retrieved list of leaves ");
			responseDto.setLeaveDto(leaveDto);
			responseDto.setMessage("Success");
			responseDto.setStatusCode(HttpStatus.OK.value());
			// responseDto.setMessage("Failed");
			// responseDto.setStatusCode(HttpStatus.EXPECTATION_FAILED.value());
		}
		return responseDto;
	}
}
