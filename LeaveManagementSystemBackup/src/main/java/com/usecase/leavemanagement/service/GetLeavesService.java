package com.usecase.leavemanagement.service;


import org.springframework.stereotype.Service;

import com.usecase.leavemanagement.dto.ResponseDto;
import com.usecase.leavemanagement.exception.LeaveException;
  
@Service
public interface GetLeavesService {

	public ResponseDto getLeavesListForParticularUser(int id) throws LeaveException;

	
}
