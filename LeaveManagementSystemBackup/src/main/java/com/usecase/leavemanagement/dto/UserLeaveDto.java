package com.usecase.leavemanagement.dto;


/**
 * This is the DTO for UserLeave.
 * @author pooja.kommi
 *
 */
public class UserLeaveDto {

	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private int userId;
	private int leaveId;
	private int daysApplied;
	
	
	public int getLeaveId() {
		return leaveId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}
	public int getDaysApplied() {
		return daysApplied;
	}
	public void setDaysApplied(int daysApplied) {
		this.daysApplied = daysApplied;
	}
	public UserLeaveDto() {
		super();
	}


}
