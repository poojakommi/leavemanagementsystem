package com.usecase.leavemanagement.dto;

import java.util.List;

/**
 * This is the DTO for the Response.
 * @author pooja.kommi
 *
 */
public class ResponseDto {

	private Integer statusCode;

	private String message;

	private List<LeaveDto> leaveDto;

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public List<LeaveDto> getLeaveDto() {
		return leaveDto;
	}

	public void setLeaveDto(List<LeaveDto> leaveDto) {
		this.leaveDto = leaveDto;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

}
