package com.usecase.leavemanagement.dto;

import com.usecase.leavemanagement.entity.User;
/**
 * This is the DTO for Leave.
 * @author pooja.kommi
 *
 */
public class LeaveDto {
	
	private int leaveId;
	private String leaveType;
	private int availableLeaves;
	public int getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public int getAvailableLeaves() {
		return availableLeaves;
	}
	public void setAvailableLeaves(int availableLeaves) {
		this.availableLeaves = availableLeaves;
	}
	
	User user;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	

}
