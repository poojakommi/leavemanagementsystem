package com.usecase.leavemanagement.dto;

import com.usecase.leavemanagement.entity.Leave;

/**
 * This is the DTO for User.
 * @author pooja.kommi
 *
 */
public class UserDto {
	
	private int userId;
	private String userName;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Boolean getIsLoggedIn() {
		return isLoggedIn;
	}
	public void setIsLoggedIn(Boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}
	public Leave getLeave() {
		return leave;
	}
	public void setLeave(Leave leave) {
		this.leave = leave;
	}
	private Boolean isLoggedIn;
	private Leave leave;

	
}
